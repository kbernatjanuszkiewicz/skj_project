import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Scanner;

import static java.lang.System.out;

public class Program {

    public static void main(String[] args) {
        Server.startServer();
        Client.startClient();
    }




    public static void sendFileThroughSocket(Socket s, int fileNumber) {
        try {
            final File folder = new File("./Dane");
            File fileToSend = folder.listFiles()[fileNumber];

            InputStream in = Files.newInputStream(fileToSend.toPath());
            OutputStream out = s.getOutputStream();

            int count;
            byte[] buffer = new byte[8192];
            while ((count = in.read(buffer)) > 0)
            {
                out.write(buffer, 0, count);
            }

            out.close();
        }catch(Exception exc) {
            out.println(exc);
        }
    }

}
