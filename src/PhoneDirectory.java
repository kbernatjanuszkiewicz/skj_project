import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PhoneDirectory {
    private Map pbMap = new HashMap();

    public PhoneDirectory(String filename) {
        try {
            BufferedReader br = new BufferedReader(
                    new FileReader(filename)
            );
            String line = br.readLine();
            while((line != null)) {
                String[] info = line.split(" +", 2);
                pbMap.put(info[0], info[1]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPhoneNumber(String name) {
        return (String) pbMap.get(name);
    }

    public boolean addPhoneNumber(String name, String num) {
        if (pbMap.containsKey(name)) return false;
        pbMap.put(name, num);
        return true;
    }

    public boolean replacePhoneNumber(String name, String num) {
        if (!pbMap.containsKey(name)) return false;
        pbMap.put(name, num);
        return true;
    }
}
