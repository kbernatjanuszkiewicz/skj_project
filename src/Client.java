import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.Date;
import java.util.Scanner;

public class Client {

    public static void javaFilesList() {
        String dirName = ".";

        try {
            Files.list(new File(dirName).toPath())
                    .limit(10)
                    .forEach(path -> {
                        System.out.println(path);
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void startClient() {


        (new Thread() {
            @Override
            public void run() {

                int filesize = 1022386;
                int bytesRead;
                int currentTot = 0;

                try {
                    Scanner sc = new Scanner(System.in);

                    while (true) {

                        Socket socket = new Socket("localhost", 60016);

                        System.out.println("CLIENT: Type command:");
                        String command = sc.nextLine();



//                        if(command.equals("PUSH") || command.equals("PULL") || command.startsWith("file")) {
//                            Socket socket = new Socket("localhost", 60016);
//                            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                        if (command.equals("PUSH")) {
                            Scanner sc2 = new Scanner(System.in);
                            System.out.println("CLIENT: Type path to file you want to send");
                            String path = sc2.nextLine();
                            File transferFile = new File (path);
                            long length = transferFile.length();

                            byte [] bytearray = new byte [(int)length];
                            InputStream in = new FileInputStream(transferFile);
                            //FileInputStream fin = new FileInputStream(transferFile);
                            //BufferedInputStream bin = new BufferedInputStream(fin);
                            //bin.read(bytearray,0,bytearray.length);
                            OutputStream out = socket.getOutputStream();
                            System.out.println("Sending Files...");

                            int count;
                            while ((count = in.read(bytearray)) > 0) {
                                out.write(bytearray, 0, count);
                            }
                            out.close();
                            in.close();
                            socket.close();
                            System.out.println("File transfer complete");
                        }


//                            out.write(command);
//                            out.newLine();
//                            out.flush();

                        if (command.startsWith("file")) {

                            InputStream in = socket.getInputStream();
                            //ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            int byteToBeRead = -1;


                            final File folder = new File("./Pobrane");
                            File newFile = new File(folder.getAbsolutePath() + File.separator + new Date().getTime());
                            FileOutputStream fs = new FileOutputStream(newFile);
                            while ((byteToBeRead = in.read()) != -1) {
                                //System.out.println(byteToBeRead);
                                fs.write(byteToBeRead);
                            }

                            fs.flush();
                            fs.close();
                        }

                        //s.close();
                    }
//                        } else if(command.equals("exit")) {
//                            break;
//                        }


//                    }



                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
