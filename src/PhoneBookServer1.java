import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.regex.Pattern;

public class PhoneBookServer1 {
    private PhoneDirectory pd = null;
    private ServerSocket ss = null;
    private BufferedReader in = null;
    private PrintWriter out = null;

    public PhoneBookServer1(PhoneDirectory pd, ServerSocket ss) {
        this.pd = pd;
        this.ss = ss;
        System.out.println("Server started");
        System.out.println("at port: " + ss.getLocalPort());
        System.out.println("bind address: " + ss.getInetAddress());

        serviceConnections();
    }

    private void serviceConnections() {
        boolean serverRunning = true;
        while (serverRunning) {
            try {
                Socket conn = ss.accept();
                System.out.println("Connection established");
                serviceRequests(conn);
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
        try { ss.close(); } catch (Exception exc) {}
    }

    private static Pattern reqPatt = Pattern.compile(" +", 3);

    private static String msg[] = { "Ok", "Invalid request", "Not found",
            "Couldn't add - entry already exists",
            "Couldn't replace non-existing entry",
    };

    private void serviceRequests(Socket connection)
            throws IOException {
        try {
            in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));
            out = new PrintWriter(
                    connection.getOutputStream(), true);

            for (String line; (line = in.readLine()) != null; ) {

                String resp;                           // odpowiedź
                String[] req = reqPatt.split(line, 3); // rozbiór zlecenia
                String cmd = req[0];                   // pierwsze słowo - polecenie

                if (cmd.equals("bye")) {        // zlecenie "bye" - koniec komunikacji
                    writeResp(0, null);
                    break;
                }
                else if (cmd.equals("get")) {   // "get" - klient chce dostać nr tel.
                    if (req.length != 2) writeResp(1, null);
                    else {
                        String phNum = (String) pd.getPhoneNumber(req[1]); // pobranie
                        if (phNum == null) writeResp(2, null);             // numeru tel.
                        else writeResp(0, phNum);                          // i zapis
                    }
                }
                else if (cmd.equals("add"))  {  // "add" - klient chce dodać numer
                    if (req.length != 3) writeResp(1, null);
                    else {
                        boolean added = pd.addPhoneNumber(req[1], req[2]); // dodany?
                        if (added) writeResp(0, null);                     // tak - ok
                        else writeResp(3, null);                           // nie
                    }
                }
                else if (cmd.equals("replace"))  {  // klient chce zmienić nr tel.
                    if (req.length != 3) writeResp(1, null);
                    else {
                        boolean replaced = pd.replacePhoneNumber(req[1], req[2]);
                        if (replaced) writeResp(0, null);
                        else writeResp(4, null);
                    }
                }
                else writeResp(1, null);             // nieznane zlecenie
            }
        } catch (Exception exc) {
            exc.printStackTrace();

        } finally {
            try {                                // zamknięcie strumieni
                in.close();                        // i gniazda
                out.close();
                connection.close();
                connection = null;
            } catch (Exception exc) { }
        }
    }

    // Przekazanie odpowiedzi klientowi poprzez zapis do strumienia
    // gniazda komuniakcyjnego
    private void writeResp(int rc, String addMsg)
            throws IOException {
        out.println(rc + " " + msg[rc]);
        if (addMsg != null) out.println(addMsg);
    }

    public static void main(String[] args) {
        PhoneDirectory pd = null;
        ServerSocket ss = null;
        try {
            String phdFileName = args[0];
            String host = args[1];
            int port = Integer.parseInt(args[2]);

            pd = new PhoneDirectory(phdFileName); // utworzenie mapy numerów z pliku

            InetSocketAddress isa = new InetSocketAddress(host, port);

            ss =  new ServerSocket();             // Utworzenie gniazda serwera
            ss.bind(isa);                         // i związanie go z adresem

        } catch(Exception exc) {
            exc.printStackTrace();
            System.exit(1);
        }
        new PhoneBookServer1(pd, ss);
    }
}
