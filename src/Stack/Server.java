package Stack;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.*;

import static java.nio.file.StandardCopyOption.*;



public class Server {

    public final static int SOCKET_PORT_1 = 5501;
    public final static int SOCKET_PORT_2 = 5502;

    public final static int PRE_DOWNLOAD_SOCKET_PORT_1 = 7777;
    public final static int PRE_DOWNLOAD_SOCKET_PORT_2 = 7778;

    public static String FILE_TO_SEND = "test.txt";
    public static String FILE_TO_RECEIVED;
    public final static int FILE_SIZE = Integer.MAX_VALUE;

    public static void deleteDowloaded() {
        String path = "./host2_receive/file-rec.txt";
        try {
            Files.delete(Paths.get(path));
            System.out.println("SERVER: content of the ./host2_receive has been deleted" );
        } catch (NoSuchFileException x) {
            System.err.format("SERVER: %s: no such" + " file or directory%n", path);
        } catch (DirectoryNotEmptyException x) {
            System.err.format("SERVER: %s not empty%n", path);
        } catch (IOException x) {
            // File permission problems are caught here.
            System.err.println(x);
        }
    }

    public static void javaFilesList(String dirName) {
        System.out.println("***************");
        System.out.println("SERVER: Show " + dirName + " content");
        try {
            Files.list(new File(dirName).toPath())
                    .limit(10)
                    .forEach(path -> {
                        System.out.println(path);
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public static void main(String[] args) throws IOException {

        int bytesRead;
        int current = 0;

        ServerSocket servsock = null;
        Socket sock = null;
        Socket secondSocket = null;

        //wysyłanie
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        OutputStream os = null;

        //odbieranie
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;

        deleteDowloaded();


        try {
            while (true) {

                ServerSocket welcomeSocket = new ServerSocket(PRE_DOWNLOAD_SOCKET_PORT_1);
                Socket clientSocket = welcomeSocket.accept();
                System.out.println("SERVER: Opening the pre-downloading socket...");
                System.out.println("SERVER: DONE");
                System.out.println("SERVER: Ready to receive the information about incoming file");
                InputStream sis = clientSocket.getInputStream();
                InputStreamReader sisr = new InputStreamReader(sis);
                BufferedReader br = new BufferedReader(sisr);
                FILE_TO_RECEIVED = br.readLine();
                System.out.println("SERVER: I've got it");
                System.out.println("SERVER: Closing the pre-downloading socket...");
                clientSocket.close();
                System.out.println("SERVER: DONE");

                servsock = new ServerSocket(SOCKET_PORT_1);

                System.out.println("SERVER: Waiting for connection...");
                try {
                    sock = servsock.accept();
                    System.out.println("SERVER: Accepted connection with: " + sock);
                    byte[] mybytearray = new byte[99999999];
                    InputStream is = sock.getInputStream();
                    fos = new FileOutputStream(FILE_TO_RECEIVED);
                    bos = new BufferedOutputStream(fos);
                    bytesRead = is.read(mybytearray, 0, mybytearray.length);
                    current = bytesRead;

                    do {
                        bytesRead =
                                is.read(mybytearray, current, (mybytearray.length - current));
                        if (bytesRead >= 0) current += bytesRead;
                    } while (bytesRead > -1);

                    bos.write(mybytearray, 0, current);
                    bos.flush();
                    System.out.println("SERVER: File " + FILE_TO_RECEIVED
                            + " downloaded (" + current + " bytes read)");




                } finally {
                    if (fos != null) fos.close();
                    if (bos != null) bos.close();
                    if (sock != null) sock.close();


                }

                break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            while(true) {
                System.out.println("Server: Opening the pre-downloading socket...");
                try {
                    secondSocket = new Socket("localhost", PRE_DOWNLOAD_SOCKET_PORT_2);
                    System.out.println("SERVER: DONE");
                    OutputStream sos = secondSocket.getOutputStream();
                    OutputStreamWriter sosw = new OutputStreamWriter(sos);
                    BufferedWriter bw = new BufferedWriter(sosw);
                    System.out.println("SERVER: Sending informations about file...");
                    bw.write(FILE_TO_SEND);
                    bw.newLine();
                    bw.flush();
                    System.out.println("SERVER: DONE");
                } finally {
                    secondSocket.close();
                }

                try {
                    sock = new Socket("localhost", SOCKET_PORT_2);
                    File myFile = new File("./host2_send/" + FILE_TO_SEND);
                    byte[] mybytearray = new byte[(int) myFile.length()];
                    fis = new FileInputStream(myFile);
                    bis = new BufferedInputStream(fis);
                    bis.read(mybytearray, 0, mybytearray.length);
                    os = sock.getOutputStream();
                    System.out.println("SERVER: Sending " + FILE_TO_SEND + "(" + mybytearray.length + " bytes)");
                    os.write(mybytearray, 0, mybytearray.length);
                    os.flush();
                    System.out.println("SERVER: Done.");
                    /*
                    try {
                        Path sourcePath = FileSystems.getDefault().getPath("file-rec.txt");
                        Path destinationPath = FileSystems.getDefault().getPath("./host2_receive/");
                        Files.move(sourcePath, destinationPath, StandardCopyOption.ATOMIC_MOVE);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    */
                    File file = new File("file-rec.txt");
                    File newFile = new File("host2_receive/file-rec.txt");
                    if(file.renameTo(newFile)){
                        System.out.println("SERVER: File move success to ./host2_receive");;
                    } else{
                        System.out.println("SERVER: File move failed");
                    }

                    javaFilesList("./host2_receive");

                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (bis != null) bis.close();
                if (os != null) os.close();
                if (sock != null) sock.close();
                break;
            }
        } finally {

        }

    }
}
