package Stack;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Scanner;

public class Client {
    public final static int SOCKET_PORT_1 = 5501;
    public final static int SOCKET_PORT_2 = 5502;
    public final static int PRE_DOWNLOAD_SOCKET_PORT_1 = 7777;
    public final static int PRE_DOWNLOAD_SOCKET_PORT_2 = 7778;
    public static String FILE_TO_RECEIVED;
    public static String FILE_TO_SEND = "file-rec.txt";

    public static void deleteDowloaded () {
        String path = "./host1_receive/test.txt";
        try {
            Files.delete(Paths.get(path));
            System.out.println("CLIENT: content of the ./host1_receive has been deleted" );
        } catch (NoSuchFileException x) {
            System.err.format("CLIENT: %s: no such" + " file or directory%n", path);
        } catch (DirectoryNotEmptyException x) {
            System.err.format("CLIENT: %s not empty%n", path);
        } catch (IOException x) {
            System.err.println(x);
        }
    }


    public static void javaFilesList(String dirName) {
        System.out.println("***************");
        System.out.println("CLIENT: Show " + dirName + " content");
        try {
            Files.list(new File(dirName).toPath())
                    .limit(10)
                    .forEach(path -> {
                        System.out.println(path);
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        deleteDowloaded();
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        ServerSocket servsock = null;
        Socket sock = null;
        Socket firstSocket = null;

        int bytesRead;
        int current = 0;

        //odbieranie
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;



        try {
            while (true) {
                System.out.println("CLIENT: Opening the pre-downloading socket...");
                try {
                    firstSocket = new Socket("localhost", PRE_DOWNLOAD_SOCKET_PORT_1);
                    System.out.println("CLIENT: DONE");
                    OutputStream sos = firstSocket.getOutputStream();
                    OutputStreamWriter sosw = new OutputStreamWriter(sos);
                    BufferedWriter bw = new BufferedWriter(sosw);
                    System.out.println("CLIENT: Sending informations about file...");
                    bw.write(FILE_TO_SEND);
                    bw.newLine();
                    bw.flush();
                    System.out.println("CLIENT: DONE");
                    firstSocket.close();
                    System.out.println("CLIENT: Pre-downloading socket has been closed");
                    System.out.println("CLIENT: Opening the downloading socket...");

                    sock = new Socket("localhost", SOCKET_PORT_1);

                    // send file
                    File myFile = new File("./host1_send/" + FILE_TO_SEND);
                    byte[] mybytearray = new byte[(int) myFile.length()];
                    fis = new FileInputStream(myFile);
                    bis = new BufferedInputStream(fis);
                    bis.read(mybytearray, 0, mybytearray.length);
                    os = sock.getOutputStream();
                    System.out.println("CLIENT: Sending " + FILE_TO_SEND + "(" + mybytearray.length + " bytes)");
                    os.write(mybytearray, 0, mybytearray.length);
                    os.flush();
                    System.out.println("CLIENT: Done.");

                } catch (IOException ex) {
                    System.out.println("CLIENT: " + ex.getMessage() + ": An Inbound Connection Was Not Resolved");
                }

                if (bis != null) bis.close();
                if (os != null) os.close();
                if (sock != null) sock.close();
                break;

            }
        } finally {
//            if (sock != null)
//                sock.close();
        }


        try {
            while(true) {
                try {
                    ServerSocket welcomeSocket = new ServerSocket(PRE_DOWNLOAD_SOCKET_PORT_2);
                    Socket clientSocket = welcomeSocket.accept();
                    System.out.println("CLIENT: Opening the pre-downloading socket...");
                    System.out.println("CLIENT: DONE");
                    System.out.println("CLIENT: Ready to receive the information about incoming file");
                    InputStream sis = clientSocket.getInputStream();
                    InputStreamReader sisr = new InputStreamReader(sis);
                    BufferedReader br = new BufferedReader(sisr);
                    FILE_TO_RECEIVED = br.readLine();
                    System.out.println("CLIENT: I've got it");
                    System.out.println("CLIENT: Closing the pre-downloading socket...");
                    clientSocket.close();
                    System.out.println("CLIENT: DONE");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    servsock = new ServerSocket(SOCKET_PORT_2);
                    sock = servsock.accept();
                    System.out.println("CLIENT: Accepted connection with: " + sock);
                    byte[] mybytearray = new byte[99999999];
                    InputStream is = sock.getInputStream();
                    fos = new FileOutputStream(FILE_TO_RECEIVED);
                    bos = new BufferedOutputStream(fos);
                    bytesRead = is.read(mybytearray, 0, mybytearray.length);
                    current = bytesRead;

                    do {
                        bytesRead =
                                is.read(mybytearray, current, (mybytearray.length - current));
                        if (bytesRead >= 0) current += bytesRead;
                    } while (bytesRead > -1);

                    bos.write(mybytearray, 0, current);
                    bos.flush();
                    System.out.println("CLIENT: File " + FILE_TO_RECEIVED
                            + " downloaded (" + current + " bytes read)");





                } finally {
                    if (fos != null) fos.close();
                    if (bos != null) bos.close();
                    if (sock != null) sock.close();

                    File file = new File("test.txt");
                    File newFile = new File("host1_receive/test.txt");
                    if(file.renameTo(newFile)){
                        System.out.println("CLIENT: File move success to ./host1_receive");;
                    } else{
                        System.out.println("CLIENT: File move failed");
                    }
                    javaFilesList("./host1_receive");
                }
                break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



