import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class DateTime {
    public static void main(String [] args) {
        String host = "time.nist.gov";
        int port = 13;

        try {


            Socket socket = new Socket(host, port);

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()
                    )
            );

            String line;
            while((line = br.readLine()) != null) {
                System.out.println(line);
            }

            br.close();
            socket.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
